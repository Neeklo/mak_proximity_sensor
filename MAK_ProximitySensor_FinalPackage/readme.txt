To install: 

sudo apt-get update
sudo apt-get upgrade
sudo apt-get python3
sudo pip3 install python-vlc

Copy MAK_Videos folder to:
	~/Desktop

Copy fehTrigger.desktop to:
	/etc/xdg/autostart

Edit cmdline.txt:
	
	add (on same line): 
	cma=512 blankscreen = 0
	to:
	/boot/cmdline.txt

Change VLC file caching buffersize to 2000ms [VLC -> Tools -> Preferences -> Show settings:"All" -> Input/Codecs, scrolldow]
alternatively, on comand line (I DIDN'T THEST THIS!):
alias vlc="vlc --filecaching=2000"

Before rebooting move the videos that needs to be played in the Desktop/MAK_Videos/Video folder (Naming is not important)
The video format reqiured is 4k mp4 H265. Any other format has not successfully be tested. Lower resolutions should instead not provide any problem.

on reboot the system will show a black screen (from -feh program) and start the sensor python program.